local lsp = require("lsp-zero")

lsp.preset({
    name = "recommended",
    set_lsp_keymaps = false,
})

lsp.ensure_installed({
    "clangd",
    "lua_ls",
    "jedi_language_server",
    "rust_analyzer",
})

-- Fix Undefined global "vim"
lsp.nvim_workspace()

lsp.configure("clangd", {
    cmd = {"clangd", "--header-insertion=never"}
})

local cmp = require("cmp")
local cmp_select = {behavior = cmp.SelectBehavior.Select}
local cmp_mappings = lsp.defaults.cmp_mappings({
  ["<C-p>"] = cmp.mapping.select_prev_item(cmp_select),
  ["<C-n>"] = cmp.mapping.select_next_item(cmp_select),
  ["<C-y>"] = cmp.mapping.confirm({ select = true }),
  ["<C-Space>"] = cmp.mapping.complete(),
})

cmp_mappings["<Tab>"] = nil
cmp_mappings["<S-Tab>"] = nil

lsp.setup_nvim_cmp({
  mapping = cmp_mappings
})

lsp.on_attach(function(client, bufnr)
    local opts = {
        buffer = bufnr,
        remap = false
    }

    vim.keymap.set("n", "gD", vim.lsp.buf.declaration, opts)

    vim.keymap.set("n", "K", vim.lsp.buf.hover, opts)

    vim.keymap.set("n", "<C-y>", vim.diagnostic.open_float, opts)
    vim.keymap.set("n", "<C-n>", vim.diagnostic.goto_next, opts)
    vim.keymap.set("n", "<C-p>", vim.diagnostic.goto_prev, opts)

    vim.keymap.set("n", "<leader>rr", vim.lsp.buf.rename, opts)
    vim.keymap.set("n", "<leader>a", vim.lsp.buf.code_action, opts)

    vim.keymap.set("n", "<leader>o", function()
      vim.lsp.buf.format { async = true }
    end, opts)
end)

lsp.setup()

vim.diagnostic.config({
    virtual_text = true
})


local builtin = require("telescope.builtin")

-- Find files.
vim.keymap.set("n", "<leader>ff", builtin.find_files, {})

-- Find in Git.
vim.keymap.set("n", "<leader>fg", builtin.git_files, {})

-- Find in help.
vim.keymap.set("n", "<leader>fh", builtin.help_tags, {})

-- Find string.
vim.keymap.set("n", "<leader>fs", builtin.live_grep, {})

-- Find with telescope.
vim.keymap.set("n", "<leader>ft", builtin.builtin, {})

-- Find symbol in document.
vim.keymap.set("n", "<leader>fd", builtin.lsp_document_symbols, {})

-- Find symbol in workspace.
vim.keymap.set("n", "<leader>fw", builtin.lsp_workspace_symbols, {})

vim.keymap.set("v", "<C-f>", builtin.grep_string, {})

vim.keymap.set("n", "gd", builtin.lsp_definitions, {})
vim.keymap.set("n", "gi", builtin.lsp_implementations, {})
vim.keymap.set("n", "gr", builtin.lsp_references, {})
vim.keymap.set("n", "gt", builtin.lsp_type_definitions, {})

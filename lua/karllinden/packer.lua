vim.cmd.packadd("packer.nvim")

return require("packer").startup(function(use)
    -- Packer can manage itself.
    use "wbthomason/packer.nvim"

    use {
        "nvim-telescope/telescope.nvim",
        requires = {
            {"nvim-tree/nvim-web-devicons"},
            {"nvim-lua/plenary.nvim"},
        }
    }

    -- Use the Onedark Warmer colorscheme.
    use {
        "navarasu/onedark.nvim",
        config = function()
            require("onedark").setup {
                style = "warmer"
            }
            require("onedark").load()
            vim.cmd("colorscheme onedark")
        end
    }

    use {
        "folke/trouble.nvim",
        requires = { {"nvim-tree/nvim-web-devicons"} }
    }

    use {
        "nvim-treesitter/nvim-treesitter",
        run = function()
            local ts_update = require("nvim-treesitter.install").update({ with_sync = true })
            ts_update()
        end
    }

    use "igankevich/mesonic"
    use "nvim-treesitter/nvim-treesitter-context"
    use "tpope/vim-fugitive"

    use {
        "VonHeikemen/lsp-zero.nvim",
        requires = {
            -- LSP Support
            {"neovim/nvim-lspconfig"},
            {"williamboman/mason.nvim"},
            {"williamboman/mason-lspconfig.nvim"},

            -- Autocompletion
            {"hrsh7th/nvim-cmp"},
            {"hrsh7th/cmp-buffer"},
            {"hrsh7th/cmp-path"},
            {"saadparwaiz1/cmp_luasnip"},
            {"hrsh7th/cmp-nvim-lsp"},
            {"hrsh7th/cmp-nvim-lua"},

            -- Snippets
            {"L3MON4D3/LuaSnip"},
            {"rafamadriz/friendly-snippets"},
        }
    }

    use {
        "kosayoda/nvim-lightbulb",
        requires = {
            {"antoinemadec/FixCursorHold.nvim"},
        }
    }
end)

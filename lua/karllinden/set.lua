vim.opt.exrc = true
vim.opt.secure = true

vim.opt.number = true
vim.opt.relativenumber = true

vim.opt.tabstop = 4
vim.opt.softtabstop = 4
vim.opt.shiftwidth = 4
vim.opt.expandtab = true

vim.opt.smartindent = true

vim.opt.swapfile = false

vim.opt.termguicolors = true

vim.opt.scrolloff = 8
vim.opt.signcolumn = "yes"

vim.opt.textwidth = 80
vim.opt.colorcolumn = "+1"

vim.opt.listchars="eol:$,tab:>-,trail:~,extends:>,precedes:<"
vim.opt.list = true

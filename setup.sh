#!/bin/bash

set -ex

mkdir "${HOME}"/Sources
cd "${HOME}"/Sources
git clone https://github.com/neovim/neovim.git
cd neovim
make CMAKE_BUILD_TYPE=Release
sudo make install
cd /usr/local/bin
sudo ln -s nvim vim

git config --global core.editor "vim"

git clone --depth 1 https://github.com/wbthomason/packer.nvim\ "${HOME}"/.local/share/nvim/site/pack/packer/start/packer.nvim
